module "code-pipeline" {
  source = "./tf-modules/codepipeline"

  profile                  = "default"
  region                   = "us-east-2"
  project_name             = "test"
  bucket_name              = "test-tf-vf"
  bitbucket_connection_arn = "arn:aws:codestar-connections:us-east-1:827828756332:connection/dafc7936-d1c9-4c84-96f3-7394a7989dc1"
  repo_name                = "vectorform/terraform-test"
  branch                   = "master"
}
