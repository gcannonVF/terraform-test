variable "profile" {
  type        = string
  description = "AWS profile to use"
}

variable "region" {
  type        = string
  description = "AWS region to use"
}

variable "project_name" {
  type        = string
  description = "Name of the project"
}

variable "bitbucket_connection_arn" {
  type        = string
  description = "ARN for the AWS CodeStar to BitBucket connection"
}

variable "repo_name" {
  type        = string
  description = "Path to repo. Format should be vectorform/<REPO>"
}

variable "branch" {
  type        = string
  description = "Branch in repo to build"
}

variable "bucket_name" {
  type        = string
  description = "Name of bucket that will host the site. example 'www.vectorform.com'"
}

provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_s3_bucket" "site-bucket" {
  bucket = "${var.bucket_name}"
  acl    = "public-read"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
		{
			"Sid": "PublicReadGetObject",
			"Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${var.bucket_name}/*"
    }
  ]
}
EOF

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = {
    Name = var.project_name
  }
}

resource "aws_s3_bucket" "codepipeline_bucket" {
  bucket = "${var.project_name}-vectorform-code-pipeline-bucket" # verbose to avoid naming collisons
  acl    = "private"
}

resource "aws_iam_role" "codepipeline_role" {
  name = "${var.project_name}-codepipeline-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codepipeline.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codepipeline_policy" {
  name = "${var.project_name}-codepipeline_policy"
  role = aws_iam_role.codepipeline_role.id

  policy = <<POLICY
{
	"Statement": [
     {
         "Action": [
             "iam:PassRole"
         ],
         "Resource": "*",
         "Effect": "Allow",
         "Condition": {
             "StringEqualsIfExists": {
                 "iam:PassedToService": [
                     "cloudformation.amazonaws.com",
                     "elasticbeanstalk.amazonaws.com",
                     "ec2.amazonaws.com",
                     "ecs-tasks.amazonaws.com"
                 ]
             }
         }
     },
     {
         "Action": [
             "codecommit:CancelUploadArchive",
             "codecommit:GetBranch",
             "codecommit:GetCommit",
             "codecommit:GetUploadArchiveStatus",
             "codecommit:UploadArchive"
         ],
         "Resource": "*",
         "Effect": "Allow"
     },
     {
         "Action": [
             "codedeploy:CreateDeployment",
             "codedeploy:GetApplication",
             "codedeploy:GetApplicationRevision",
             "codedeploy:GetDeployment",
             "codedeploy:GetDeploymentConfig",
             "codedeploy:RegisterApplicationRevision"
         ],
         "Resource": "*",
         "Effect": "Allow"
     },
     {
         "Action": [
             "codestar-connections:UseConnection"
         ],
         "Resource": "*",
         "Effect": "Allow"
     },
     {
         "Action": [
             "elasticbeanstalk:*",
             "ec2:*",
             "elasticloadbalancing:*",
             "autoscaling:*",
             "cloudwatch:*",
             "s3:*",
             "sns:*",
             "cloudformation:*",
             "rds:*",
             "sqs:*",
             "ecs:*"
         ],
         "Resource": "*",
         "Effect": "Allow"
     },
     {
         "Action": [
             "lambda:InvokeFunction",
             "lambda:ListFunctions"
         ],
         "Resource": "*",
         "Effect": "Allow"
     },
     {
         "Action": [
             "opsworks:CreateDeployment",
             "opsworks:DescribeApps",
             "opsworks:DescribeCommands",
             "opsworks:DescribeDeployments",
             "opsworks:DescribeInstances",
             "opsworks:DescribeStacks",
             "opsworks:UpdateApp",
             "opsworks:UpdateStack"
         ],
         "Resource": "*",
         "Effect": "Allow"
     },
     {
         "Action": [
             "cloudformation:CreateStack",
             "cloudformation:DeleteStack",
             "cloudformation:DescribeStacks",
             "cloudformation:UpdateStack",
             "cloudformation:CreateChangeSet",
             "cloudformation:DeleteChangeSet",
             "cloudformation:DescribeChangeSet",
             "cloudformation:ExecuteChangeSet",
             "cloudformation:SetStackPolicy",
             "cloudformation:ValidateTemplate"
         ],
         "Resource": "*",
         "Effect": "Allow"
     },
     {
         "Action": [
             "codebuild:BatchGetBuilds",
             "codebuild:StartBuild"
         ],
         "Resource": "*",
         "Effect": "Allow"
     },
     {
         "Effect": "Allow",
         "Action": [
             "devicefarm:ListProjects",
             "devicefarm:ListDevicePools",
             "devicefarm:GetRun",
             "devicefarm:GetUpload",
             "devicefarm:CreateUpload",
             "devicefarm:ScheduleRun"
         ],
         "Resource": "*"
     },
     {
         "Effect": "Allow",
         "Action": [
             "servicecatalog:ListProvisioningArtifacts",
             "servicecatalog:CreateProvisioningArtifact",
             "servicecatalog:DescribeProvisioningArtifact",
             "servicecatalog:DeleteProvisioningArtifact",
             "servicecatalog:UpdateProduct"
         ],
         "Resource": "*"
     },
     {
         "Effect": "Allow",
         "Action": [
             "cloudformation:ValidateTemplate"
         ],
         "Resource": "*"
     },
     {
         "Effect": "Allow",
         "Action": [
             "ecr:DescribeImages"
         ],
         "Resource": "*"
     }
 ],
 "Version": "2012-10-17"
}
POLICY
}

resource "aws_iam_role" "codebuild_role" {
  name = "${var.project_name}-codebuild-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codebuild-policy" {
  role = aws_iam_role.codebuild_role.name

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": [
        "*"
      ],
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:CreateNetworkInterface",
        "ec2:DescribeDhcpOptions",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeVpcs"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
       "*" 
      ]
    }
  ]
}
POLICY
}



resource "aws_codebuild_project" "codebuild" {
  name         = "${var.project_name}-codebuild"
  service_role = aws_iam_role.codebuild_role.name

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:2.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
  }

  source {
    type                = "CODEPIPELINE"
    report_build_status = true
  }
}


resource "aws_codepipeline" "codepipeline" {
  name     = "${var.project_name}-pipeline"
  role_arn = aws_iam_role.codepipeline_role.arn

  artifact_store {
    location = aws_s3_bucket.codepipeline_bucket.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeStarSourceConnection"
      version          = "1"
      output_artifacts = ["source_output"]

      configuration = {
        ConnectionArn    = var.bitbucket_connection_arn
        FullRepositoryId = var.repo_name
        BranchName       = var.branch
      }
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output"]
      output_artifacts = ["build_output"]
      version          = "1"

      configuration = {
        ProjectName = aws_codebuild_project.codebuild.name
      }
    }
  }

  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "S3"
      input_artifacts = ["build_output"]
      version         = "1"

      configuration = {
        BucketName = aws_s3_bucket.site-bucket.bucket
        Extract    = true
      }
    }
  }
}
